FROM node:carbon

#install dictionary for gbo/define commands
RUN apt-get update && apt-get install -y wamerican-small

WORKDIR /app/

#copy in package-lock and package.json and install deps (do this before src copy so that layers cache)
COPY yarn.lock package.json /app/
RUN yarn

#copy in src and other required files, then build the client/server
COPY . /app/
RUN yarn build

VOLUME /app/storage

EXPOSE 80


CMD yarn start:server