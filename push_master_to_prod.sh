#!/bin/bash
set -e

ssh -t gbo@gebeo.xyz 'sudo docker pull professionalprogrammingers/gebeo:master; sudo docker tag professionalprogrammingers/gebeo:master professionalprogrammingers/gebeo:prod; sudo docker push professionalprogrammingers/gebeo:prod; sudo docker stop gebeo; sudo docker rm gebeo; sudo docker run -p 80:8080 --restart unless-stopped -v ~/volumes/storage:/app/storage --name gebeo -d professionalprogrammingers/gebeo:prod'
