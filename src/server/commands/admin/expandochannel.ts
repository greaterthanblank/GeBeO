import * as Commando from 'discord.js-commando';
import * as Discord from 'discord.js';


module.exports = class ExpandoPrefixCommand extends Commando.Command {
  constructor(client: Commando.CommandoClient) {
    let commandInfo: any = {
      name: 'expandochannel',
      group: 'admin',
      memberName: 'expandochannel',
      description: 'Create a new expando channel.',
      userPermissions: ['ADMINISTRATOR'],
      argsType: 'single',
    }
    super(client, commandInfo);
  }

  async run(msg: Commando.CommandMessage, arg: string): Promise<Discord.Message | Discord.Message[]> {
    let prefix: string = (msg.guild as any).settings.get('expando_name_prefix', '🎮 ') as string;
    let name: string = (msg.guild as any).settings.get('expando_default_name', 'Game Room') as string;
    
    await msg.guild.createChannel(prefix + name, 'voice');
    return;
  }
}
