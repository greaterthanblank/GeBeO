import * as Commando from 'discord.js-commando';
import * as Discord from 'discord.js';
const downloader: any = require('@microlink/youtube-dl/lib/downloader');


module.exports = class UpdateYTDLCommand extends Commando.Command {
    constructor(client: Commando.CommandoClient) {
        super(client, {
            name: 'updateytdl',
            group: 'admin',
            memberName: 'updateytdl',
            description: 'Try and udpate YouTube-DL',
            ownerOnly: true
        })
    }

    async run(msg: Commando.CommandMessage): Promise<Discord.Message | Discord.Message[]> {
        downloader((err: any, done: any) => {
            if (err) {
                return console.log(err.stack);
            }
            console.log(done);
        })
        return msg.reply('Trying to download a new version of ytdl.')
    }
}
