import * as Discord from 'discord.js';
import * as Commando from 'discord.js-commando';
import * as winston from 'winston';
import * as fh from 'utils/file_helper';
import * as fs from 'fs';
import {RandRoleColorCfg} from 'utils/shared_config';

const offset: number = -8;

function getPSTTime(): Date {
    let d: Date = new Date();
    let utc: number = d.getTime() + (d.getTimezoneOffset() * 60000);
    let nd: Date = new Date(utc + (3600000 * offset));
    return nd;
}

var lastWeekday: number = getPSTTime().getDay();

async function dayDetector(client: Commando.CommandoClient) {
    let currentTime: Date = getPSTTime();

    await statusUpdate(currentTime, client);

    let currentWeekday = currentTime.getDay();
    if (currentWeekday != lastWeekday) {
        lastWeekday = currentWeekday;
        await roleColorRanomizer(client);
        await wednesdayDetector(currentTime, client);
    }
}

async function roleColorRanomizer(client: Commando.CommandoClient) {
    winston.info('role color randomizer activated')
    client.guilds.forEach((guild: Discord.Guild, key: string) => {
        let roleFile = fh.getGuildDir(guild.id) + 'randrolecolor.json';
        if (!fs.existsSync(roleFile)) {
            return;
        }

        let roleCfg: RandRoleColorCfg = JSON.parse(fs.readFileSync(roleFile, 'utf8'));
        if (roleCfg.colors.length == 0) {
            return;
        }

        roleCfg.roles.forEach((roleId: string) => {
            let role: Discord.Role = guild.roles.get(roleId);
            if (role == null) {
                return;
            }

            let colorInd: number = Math.floor(Math.random() * roleCfg.colors.length);
            role.setColor(roleCfg.colors[colorInd]);
        });
    })
}

async function wednesdayDetector(currTime: Date, client: Commando.CommandoClient) {
    if (currTime.getDay() == 3) {
        winston.info('day detector activated')
        let myDudes = '<:MyDudes:304341572168712193> ';
        let msg = myDudes.repeat(3) + 'It is Wednesday my dudes ' + myDudes.repeat(3);
        client.guilds.forEach((value: Discord.Guild, key: string) => {
            let channelId: string = client.provider.get(value, 'wed_detector_channel', null);
            if (channelId == null) return;
            let channel: Discord.GuildChannel = value.channels.get(channelId)
            if (channel.type == 'text') {
                (channel as Discord.TextChannel).send(msg, new Discord.Attachment('./resources/wednesdayfrog.jpg'));
            }
        })
    }
}

async function statusUpdate(currentTime: any, client: Commando.CommandoClient) {
    if (currentTime.getMonth() == 11 && currentTime.getDate() < 26) { 
        let christmasOff: Date = new Date(currentTime.getFullYear(), currentTime.getMonth(), 25, 0, 0, 0, 0);
        let christmasUtc: number = christmasOff.getTime() + (christmasOff.getTimezoneOffset() * 60000);
        let christmas: any = new Date(christmasUtc + (3600000 * offset));
        let daysUntilChristmas = Math.floor((christmas - currentTime) / 86400000) + 1;
        await client.user.setPresence({game: {name: daysUntilChristmas.toString() + ' days to Christmas!'}})
    } else {
        let yearDateRangeMS: number = new Date(currentTime.getFullYear() + 1, 0).getTime() - new Date(currentTime.getFullYear(), 0).getTime()
        let todayDateRangeMS: number = currentTime.getTime() - new Date(currentTime.getFullYear(), 0).getTime();
        let percentComplete = todayDateRangeMS / yearDateRangeMS * 100;
        await client.user.setPresence({game: {name: percentComplete.toFixed(3) + '% thru ' + currentTime.getFullYear() + '!'}})
    }
}

module.exports = function(client: Commando.CommandoClient) {
    lastWeekday = getPSTTime().getDay();
    client.on('ready', () =>  {
        dayDetector(client);
        setInterval(dayDetector, 60000, client)
    });
}